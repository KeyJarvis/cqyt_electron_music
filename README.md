

# B2音乐播放器

> 一个基于 electron-vue 开发的音乐播放器

#### 关于作者

``` 
大四学生狗一枚，坐标重庆，毕业设计作品。
```

##### 运行项目

``` bash
git clone https://gitlab.com/KeyJarvis/cqyt_electron_music

cd electron-vue-music

npm install

# 运行开发模式
npm run dev

# 打包安装文件
npm run build
```
>注意 正式打包需要把图标样式下载到本地，否则正式版图标不显示

#### 网易云音乐 API
<a href="https://binaryify.github.io/NeteaseCloudMusicApi/#/">https://binaryify.github.io/NeteaseCloudMusicApi/#/</a>
> 已经集成到 electron  


#### 关于解释说明

  本作品借鉴了<a href="https://juejin.im/post/5b5595a9f265da0f652387d7">小土狗</a>的作品，十分感谢，对我的帮助很大。
